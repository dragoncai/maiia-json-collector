package org.example.post.collector;

import org.example.post.collector.backend.PostProvider;
import org.example.post.collector.backend.PostRepository;
import org.example.post.collector.service.PostsCollectorService;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.Contract;

@Configuration
@EnableFeignClients
public class PostCollectorConfiguration {
  @Bean
  public PostsCollectorService service(PostProvider postProvider, PostRepository postRepository) {
    return new PostsCollectorService(postProvider, postRepository);
  }

  @Bean
  public Contract feignContract() {
    return new feign.Contract.Default();
  }
}
