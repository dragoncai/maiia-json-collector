package org.example.post.collector.service;

import java.util.List;
import java.util.stream.Collectors;

import org.example.post.collector.backend.PostProvider;
import org.example.post.collector.backend.PostRepository;
import org.example.post.collector.model.PostRest;
import org.springframework.data.domain.PageRequest;

public final class PostsCollectorService {
  private final PostProvider postProvider;
  private final PostRepository postRepository;

  public PostsCollectorService(PostProvider postProvider, PostRepository postRepository) {
    this.postProvider = postProvider;
    this.postRepository = postRepository;
  }

  public List<PostRest> collect(int size) {
    return postRepository.findAllByOrderByTitle(PageRequest.of(0, size)).stream().collect(Collectors.toUnmodifiableList());
  }

  public List<PostRest> publish() {
    List<PostRest> posts = postProvider.posts();
    postRepository.saveAll(posts);
    return posts;
  }
}
