package org.example.post.collector.backend;

import java.util.List;

import org.example.post.collector.model.PostRest;
import org.springframework.cloud.openfeign.FeignClient;

import feign.RequestLine;

@FeignClient(name = "post-provider", url = "https://jsonplaceholder.typicode.com/")
public interface PostProvider {
  @RequestLine("GET /posts")
  List<PostRest> posts();
}
