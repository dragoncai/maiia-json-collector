package org.example.post.collector.backend;

import java.util.List;

import org.example.post.collector.model.PostRest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PostRepository extends MongoRepository<PostRest, Integer> {
  //Note that order is [A..Za..z] and not [Aa..Zz]
  //Otherwise a workaround would be to add a column in the table with the first letter of the title being case insensitive
  List<PostRest> findAllByOrderByTitle(Pageable pageable);
}
