package org.example.post.collector.model;

import java.util.List;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@Value.Style(typeAbstract = "*Definition", typeImmutable = "*", visibility = Value.Style.ImplementationVisibility.PUBLIC)
@JsonSerialize(as = PostsRest.class)
@JsonDeserialize(as = PostsRest.class)
interface PostsRestDefinition {
  @Value.Default
  default int totalCount() {
    return posts().size();
  }

  List<PostRest> posts();
}
