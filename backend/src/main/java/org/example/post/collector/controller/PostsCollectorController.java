package org.example.post.collector.controller;

import org.example.post.collector.model.PostsRest;
import org.example.post.collector.service.PostsCollectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public final class PostsCollectorController {
  @Autowired
  private PostsCollectorService service;

  @PostMapping("/publish")
  public PostsRest publish() {
    return PostsRest.builder().posts(service.publish()).build();
  }

  @GetMapping("/posts")
  public PostsRest getPost() {
    return PostsRest.builder().posts(service.collect(50)).build();
  }

}
