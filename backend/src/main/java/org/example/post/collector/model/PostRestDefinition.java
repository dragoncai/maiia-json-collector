package org.example.post.collector.model;

import org.immutables.value.Value;
import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@Value.Style(typeAbstract = "*Definition", typeImmutable = "*", visibility = Value.Style.ImplementationVisibility.PUBLIC)
@JsonSerialize(as = PostRest.class)
@JsonDeserialize(as = PostRest.class)
interface PostRestDefinition {
  int userId();

  @Id
  int id();

  String title();

  String body();
}
