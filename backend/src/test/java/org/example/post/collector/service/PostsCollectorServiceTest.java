package org.example.post.collector.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.example.post.collector.backend.PostProvider;
import org.example.post.collector.backend.PostRepository;
import org.example.post.collector.model.PostRest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.google.common.collect.Lists;


@DataMongoTest
@ExtendWith(SpringExtension.class)
final class PostsCollectorServiceTest {
  @Autowired
  private PostRepository postRepository;

  private PostProvider postProvider;
  private PostsCollectorService postsCollectorService;

  @BeforeEach
  void setUp() {
    postRepository.deleteAll();
    postProvider = mock(PostProvider.class);
    postsCollectorService = new PostsCollectorService(postProvider, postRepository);
  }

  @Test
  void publishIsReturningAllSavedPosts() {
    PostRest post1Z = PostRest.builder().id(1).userId(1).body("").title("Z").build();
    PostRest post2A = post1Z.withTitle("A").withId(2);
    PostRest post3m = post1Z.withTitle("m").withId(3);
    ArrayList<PostRest> postRests = Lists.newArrayList(post1Z, post2A, post3m);
    when(postProvider.posts()).thenReturn(postRests);
    List<PostRest> result = postsCollectorService.publish();
    Assertions.assertIterableEquals(postRests, result);
    Assertions.assertIterableEquals(postRests, postRepository.findAll());
  }

  @Test
  void collectOneShouldReturnOnlyTheFirstAlphabeticallyOrdered() {
    PostRest post1Z = PostRest.builder().id(1).userId(1).body("").title("Z").build();
    PostRest post2A = post1Z.withTitle("A").withId(2);
    PostRest post3m = post1Z.withTitle("m").withId(3);
    ArrayList<PostRest> postRests = Lists.newArrayList(post1Z, post2A, post3m);
    postRepository.saveAll(postRests);
    List<PostRest> result = postsCollectorService.collect(1);
    Assertions.assertIterableEquals(Lists.newArrayList(post2A), result);
  }

  // [Z, A, m] => [A, Z, m]
  @Test
  void collectFiftyShouldReturnFiftyOrLess() {
    PostRest post1Z = PostRest.builder().id(1).userId(1).body("").title("Z").build();
    PostRest post2A = post1Z.withTitle("A").withId(2);
    PostRest post3m = post1Z.withTitle("m").withId(3);
    ArrayList<PostRest> postRests = Lists.newArrayList(post1Z, post2A, post3m);
    postRepository.saveAll(postRests);
    List<PostRest> result = postsCollectorService.collect(50);
    Assertions.assertIterableEquals(Lists.newArrayList(post2A, post1Z, post3m), result);
  }
}