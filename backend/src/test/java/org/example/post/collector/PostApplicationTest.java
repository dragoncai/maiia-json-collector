package org.example.post.collector;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Comparator;

import org.example.post.collector.backend.PostRepository;
import org.example.post.collector.model.PostRest;
import org.example.post.collector.model.PostsRest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Ordering;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = PostApplication.class)
@AutoConfigureMockMvc
final class PostApplicationTest {
  @Autowired
  private MockMvc mvc;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private PostRepository postRepository;

  @BeforeEach
  void setUp() {
    //reset db before each test
    postRepository.deleteAll();
  }

  @Test
  void testInitialGetPostsReturnEmpty() throws Exception {
    MvcResult mvcResult = mvc.perform(get("/posts"))
            .andExpect(status().isOk()).andReturn();
    PostsRest postsRest = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), PostsRest.class);
    Assertions.assertTrue(postsRest.posts().isEmpty());
  }

  @Test
  void testPublish() throws Exception {
    MvcResult mvcResult = mvc.perform(post("/publish"))
            .andExpect(status().isOk()).andReturn();
    PostsRest postsRest = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), PostsRest.class);
    Assertions.assertFalse(postsRest.posts().isEmpty());
    Assertions.assertEquals(100, postsRest.posts().size());
  }

  @Test
  void testPublishAndGet50SortedPosts() throws Exception {
    mvc.perform(post("/publish"));
    MvcResult mvcResult = mvc.perform(get("/posts"))
            .andExpect(status().isOk()).andReturn();
    PostsRest postsRest = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), PostsRest.class);
    Assertions.assertFalse(postsRest.posts().isEmpty());
    Assertions.assertEquals(50, postsRest.posts().size());
    Assertions.assertTrue(Ordering.from(Comparator.comparing(PostRest::title)).isOrdered(postsRest.posts()));
  }
}