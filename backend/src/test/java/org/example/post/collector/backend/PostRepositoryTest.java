package org.example.post.collector.backend;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.example.post.collector.model.PostRest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.domain.PageRequest;

import com.google.common.collect.Lists;

@DataMongoTest
final class PostRepositoryTest {
  @Autowired
  private PostRepository postRepository;

  @Test
  void testFindAllSortedAndLimit() {
    PostRest post1Z = PostRest.builder().id(1).userId(1).body("").title("Z").build();
    PostRest post2A = post1Z.withTitle("A").withId(2);
    PostRest post3m = post1Z.withTitle("m").withId(3);
    postRepository.saveAll(Lists.newArrayList(post1Z, post2A, post3m));
    List<PostRest> allByOrderByTitle = postRepository.findAllByOrderByTitle(PageRequest.of(0, 5));
    ArrayList<PostRest> expectedOrder = Lists.newArrayList(post2A, post1Z, post3m);
    Assertions.assertIterableEquals(expectedOrder, allByOrderByTitle);

  }
}