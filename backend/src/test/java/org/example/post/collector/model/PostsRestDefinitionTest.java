package org.example.post.collector.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

final class PostsRestDefinitionTest {
  @Test
  void totalCountIsZeroIfEmpty() {
    assertEquals(0, PostsRest.builder().build().totalCount());
  }

  @Test
  void lengthShouldBeEqualToTotalCount() {
    PostRest mock1 = PostRest.builder().userId(1).id(1).title("").body("").build();
    PostRest mock2 = mock1.withId(2);
    assertEquals(2, PostsRest.builder().addPosts(mock1, mock2).build().totalCount());

  }
}