import axios from 'axios'

const CLIENT = axios.create({
    baseURL: `/api`,
    timeout: 1000,
});


export default {
    getPosts() {
        return CLIENT.get(`/posts`);
    },
    publish() {
        return CLIENT.post(`/publish`);
    }
}